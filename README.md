# set-cover-problem (problem pokrycia zbioru)

##  Opis problemu
Problem pokrycia zbioru to problem optymalizacyjny stanowiący model dla wielu zadań związanych z doborem zasobów. Problem jest zaliczany do problemów NP trudnych.

Dany jest:

- skończony zbiór U elementów U = {u1, u2, ... , un}.
- rodzina S podzbiorów U taka, że każdy element zbioru U należy do przynajmniej jednego podzbioru z tej rodziny (S1, S2, ..., Sk ⊆𝑈)

Problem polega na znalezieniu podrodziny C rodziny S (minimalnego rozmiaru), której elementy pokrywają cały zbiór U

### Przykład
Dla następujących zbiorów:

U={1,2,3,4,5}  
S1={1,2,3}  
S2={2,4}  
S3={3,4}  
S4={4,5}  
S={S1,S2,S3,S4}

Pokrycie zapewniają zbiory {S1,S4}

### Opis algorytmu dokładnego
1. Utworzenie wszystkich podzbiorów zbioru S (wszystkie możliwe kombinacje)
2. Wybranie podzbiorów, które będą stanowiły pokrycie zbioru U
3. Wybranie podzbioru o najmniejszej mocy (spośród podzbiorów z punktu 2)

### Opis algorytmu aproksymacyjnego

![greedyAlgorithm.JPG](readmeImages/greedyAlgorithm.JPG)


##  Opis projektu
Projekt zawiera implementację problemu pokrycia zbioru. Projekt zawiera algorytm dokładny rozwiązujący ten problem oraz algorytm przybliżony. Dodatkowo algorytmy zostały ze sobą porównane pod względem złożoności czasowej oraz pojemnościowej. 