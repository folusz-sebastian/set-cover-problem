package model;

import org.apache.commons.lang3.RandomStringUtils;
import java.util.*;

public class SetWithName<E> extends TreeSet<E> implements Comparable<E>{
    protected String name;
    private static final String NAME_PREFIX = "set_";

    public SetWithName() {
        this.name = NAME_PREFIX + RandomStringUtils.randomAlphanumeric(4);
    }

    public SetWithName(String name) {
        this.name = name;
    }

    public SetWithName(Comparator<? super E> comparator) {
        super(comparator);
        this.name = NAME_PREFIX + RandomStringUtils.randomAlphanumeric(4);
    }

    public SetWithName( String name, Comparator<? super E> comparator) {
        super(comparator);
        this.name = name;
    }

    public SetWithName(Collection<? extends E> c) {
        super(c);
        this.name = NAME_PREFIX + RandomStringUtils.randomAlphanumeric(4);
    }

    public SetWithName(String name, Collection<? extends E> c) {
        super(c);
        this.name = name;
    }

    public SetWithName(SetWithName<E> s) {
        super(s);
        this.name = s.getName();
    }

    public SetWithName( String name, SortedSet<E> s) {
        super(s);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SetWithName<?> that = (SetWithName<?>) o;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name);
    }

    @Override
    public int compareTo(E e) {
        if (this == e) return 0;
        if (e == null) return -1;
        if (!super.equals(e)) return -1;
        SetWithName<?> that = (SetWithName<?>) e;
        return name.equals(that.name) ? 0 : -1;
    }
}
