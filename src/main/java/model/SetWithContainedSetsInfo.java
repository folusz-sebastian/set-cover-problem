package model;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class SetWithContainedSetsInfo<E> extends SetWithName<E> {
    private static final Logger LOGGER = Logger.getLogger("SetWithContainedSetsInfo");
    private final Set<SetWithName<E>> containedSets;

    public SetWithContainedSetsInfo(String name) {
        super(name);
        containedSets = new TreeSet<>(Collections.singletonList(this));
    }

    public SetWithContainedSetsInfo(String name, Collection<? extends E> collection) {
        super(name, collection);
        containedSets = new TreeSet<>(Collections.singletonList(this));
    }

    public SetWithContainedSetsInfo(SetWithContainedSetsInfo<E> set) {
        super(set);
        containedSets = new TreeSet<>(set.containedSets);
    }

    public boolean addAll(SetWithName<E> c) {
        this.containedSets.add(c);
        return super.addAll(c);
    }

    public void printContainedSetsWithSetElements(boolean printEmptySet) {
        String s = String.format("%s -> %s", containedSetsNames(printEmptySet), this);
        LOGGER.log(Level.INFO, s);
    }

    public void printContainedSets(boolean printEmptySet) {
        LOGGER.log(Level.INFO, "{0}", containedSetsNames(printEmptySet));
    }

    private String containedSetsNames(boolean printEmptySet) {
        return this.containedSets
                .stream()
                .map(SetWithName::getName)
                .filter(name -> printEmptySet || !name.equals(SetState.EMPTY.getValue()))
                .collect(Collectors.joining(", "));
    }

    public Set<SetWithName<E>> getContainedSets() {
        return containedSets;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SetWithContainedSetsInfo<?> that = (SetWithContainedSetsInfo<?>) o;
        return containedSets.equals(that.containedSets);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), containedSets);
    }
}
