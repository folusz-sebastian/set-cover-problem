package model;

public enum SetState {
    EMPTY("fi");

    private final String value;

    SetState(String s) {
        this.value = s;
    }

    public String getValue() {
        return value;
    }
}
