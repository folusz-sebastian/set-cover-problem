package main;

import algorithms.ExactAlgorithm;
import algorithms.GreedyAlgorithm;
import generators.SetsGenerator;
import model.SetWithName;
import org.openjdk.jmh.runner.RunnerException;

import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    private static final Logger LOGGER = Logger.getLogger(Main.class.getSimpleName());

    public static void main(String[] args) throws IOException, RunnerException {
        runExample();
        countTimeComplexity(args);
    }

    private static void runExample() {
        SetsGenerator sg = new SetsGenerator(10, 1, 10);

        if(!sg.isProblemCalculable())
            throw new IllegalArgumentException("Źle zdefiniowane warunki zadania - zbiór U nie może zostać wypełniony unikalnymi wartościami.");

        Set<Integer> u = sg.getU();
        List<SetWithName<Integer>> subsetsList = sg.getSubsetsList();

        LOGGER.info("Zbiór U: ");
        LOGGER.info(u.toString());

        LOGGER.info("\nLista zbiorów S: ");
        for(SetWithName<Integer> si : subsetsList)
            LOGGER.info(si.getName() + " " + si);

        LOGGER.info("Exact algorithm:");
        long startMemoryCapacity = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        ExactAlgorithm<Integer> exactAlgorithm = new ExactAlgorithm<>(subsetsList, u);
        exactAlgorithm.minSubsetThatCoverSet().printContainedSetsWithSetElements(true);
        LOGGER.log(Level.INFO,
                "space complexity for exact algorithm: {0} B", countedSpaceComplexity(startMemoryCapacity)
        );

        LOGGER.info("Generated subsets using exact algorithm:");
        exactAlgorithm.getAllPossibleSubsets().forEach(subset ->
                subset.printContainedSetsWithSetElements(true)
        );
        LOGGER.info("-----------------------------------------");

        LOGGER.info("Greedy algorithm:");
        startMemoryCapacity = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        GreedyAlgorithm<Integer> greedyAlgorithm = new GreedyAlgorithm<>(subsetsList, u);
        greedyAlgorithm.minSubsetThatCoverSet().printContainedSetsWithSetElements(true);
        LOGGER.log(Level.INFO,
                "space complexity for greedy algorithm: {0} B", countedSpaceComplexity(startMemoryCapacity)
        );
    }

    private static long countedSpaceComplexity(long startMemoryCapacity) {
        long endMemoryCapacity = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        return endMemoryCapacity - startMemoryCapacity;
    }

    private static void countTimeComplexity(String[] args) throws IOException, RunnerException {
        org.openjdk.jmh.Main.main(args);
    }
}
