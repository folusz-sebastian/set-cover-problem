package benchmark;

import algorithms.ExactAlgorithm;
import algorithms.GreedyAlgorithm;
import model.SetWithName;
import org.openjdk.jmh.annotations.*;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

@Fork(value = 1)
@Warmup(iterations = 3)
@Measurement(iterations = 4)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MICROSECONDS)
public class Benchmark {

    @org.openjdk.jmh.annotations.State(Scope.Benchmark)
    public static class State {
        Set<Integer> u = new TreeSet<>(Arrays.asList(1, 2, 3, 4, 5));
        SetWithName<Integer> s1 = new SetWithName<>("s1", Arrays.asList(1, 2, 3));
        SetWithName<Integer> s2 = new SetWithName<>("s2", Arrays.asList(2, 4));
        SetWithName<Integer> s3 = new SetWithName<>("s3", Arrays.asList(3, 4));
        SetWithName<Integer> s4 = new SetWithName<>("s4", Arrays.asList(4, 5));

        List<SetWithName<Integer>> setList = Arrays.asList(s1, s2, s3, s4);

        ExactAlgorithm<Integer> exactAlgorithm = new ExactAlgorithm<>(setList, u);
        GreedyAlgorithm<Integer> greedyAlgorithm = new GreedyAlgorithm<>(setList, u);
    }

    @org.openjdk.jmh.annotations.Benchmark
    public void testExactAlgorithm(Benchmark.State state) {
        state.exactAlgorithm.minSubsetThatCoverSet();
    }

    @org.openjdk.jmh.annotations.Benchmark
    public void testGreedyAlgorithm(Benchmark.State state) {
        state.greedyAlgorithm.minSubsetThatCoverSet();
    }
}
