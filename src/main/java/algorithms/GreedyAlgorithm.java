package algorithms;

import model.SetState;
import model.SetWithContainedSetsInfo;
import model.SetWithName;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class GreedyAlgorithm<E> extends SetCoverProblem<E> {

    public GreedyAlgorithm(List<SetWithName<E>> setList, Set<E> universe) {
        super(setList, universe);
    }

    @Override
    public SetWithContainedSetsInfo<E> minSubsetThatCoverSet() {
        SetWithContainedSetsInfo<E> minSubsetThatCoverSet = new SetWithContainedSetsInfo<>(SetState.EMPTY.getValue());
        while (!universe.isEmpty()) {
            SetWithName<E> choseSet = setList.get(
                    indexOfSetWithMaxCardinality(setsIntersectedWithUniverse())
            );
            this.universe.removeAll(choseSet);
            minSubsetThatCoverSet.addAll(choseSet);
        }
        return minSubsetThatCoverSet;
    }

    private List<Set<E>> setsIntersectedWithUniverse() {
        return setList
                .stream()
                .map(set -> setsIntersection(set, universe))
                .collect(Collectors.toList());
    }

    private Set<E> setsIntersection(Set<E> setA, Set<E> setB) {
        return setA.stream()
                .filter(setB::contains)
                .collect(Collectors.toSet());
    }

    private int indexOfSetWithMaxCardinality(List<Set<E>> setList) {
        return IntStream.range(0, setList.size())
                .boxed()
                .max(Comparator.comparing(i -> setList.get(i).size()))
                .orElseThrow(NoSuchElementException::new);
    }
}
