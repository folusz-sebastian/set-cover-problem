package algorithms;

import model.SetWithContainedSetsInfo;
import model.SetWithName;

import java.util.List;
import java.util.Set;

public abstract class SetCoverProblem<E> {
    protected List<SetWithName<E>> setList;
    protected Set<E> universe;

    public SetCoverProblem(List<SetWithName<E>> setList, Set<E> universe) {
        this.setList = setList;
        this.universe = universe;
    }

    public abstract SetWithContainedSetsInfo<E> minSubsetThatCoverSet();
}
