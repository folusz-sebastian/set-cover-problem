package algorithms;

import model.SetWithContainedSetsInfo;
import model.SetWithName;
import java.util.*;

public class ExactAlgorithm<E> extends SetCoverProblem<E> {
    private final List<SetWithContainedSetsInfo<E>> allPossibleSubsets;

    public ExactAlgorithm(List<SetWithName<E>> setList, Set<E> universe) {
        super(setList, universe);
        SubsetsGenerator<E> subsetsGenerator = new SubsetsGenerator<>(setList);
        this.allPossibleSubsets = subsetsGenerator.generateAllPossibleSubsets();
    }

    @Override
    public SetWithContainedSetsInfo<E> minSubsetThatCoverSet() {
        return this.allPossibleSubsets.stream()
                        .filter(subset -> subset.containsAll(this.universe))
                        .min(Comparator.comparing(subset -> subset.getContainedSets().size()))
                        .orElseThrow(NoSuchElementException::new);
    }

    public List<SetWithContainedSetsInfo<E>> getAllPossibleSubsets() {
        return allPossibleSubsets;
    }
}
