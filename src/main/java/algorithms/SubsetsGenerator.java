package algorithms;

import model.SetState;
import model.SetWithContainedSetsInfo;
import model.SetWithName;
import java.util.ArrayList;
import java.util.List;

public class SubsetsGenerator<E> {
    private final List<SetWithName<E>> setList;

    public SubsetsGenerator(List<SetWithName<E>> setList) {
        this.setList = setList;
    }

    public List<SetWithContainedSetsInfo<E>> generateAllPossibleSubsets() {
        return generateAllPossibleSubsets(setList.size() - 1);
    }

    private ArrayList<SetWithContainedSetsInfo<E>> generateAllPossibleSubsets(int index) {
        ArrayList<SetWithContainedSetsInfo<E>> allSubsets;
        if (index < 0) {
            allSubsets = new ArrayList<>();
            allSubsets.add(new SetWithContainedSetsInfo<>(SetState.EMPTY.getValue()));
        } else {
            allSubsets = generateAllPossibleSubsets(index - 1);

            List<SetWithContainedSetsInfo<E>> subsetsWithNewItem =
                    subsetsCreatedByAddingNewItemToExistingSubsets(allSubsets, this.setList.get(index));
            allSubsets.addAll(subsetsWithNewItem);
        }
        return allSubsets;
    }

    private ArrayList<SetWithContainedSetsInfo<E>> subsetsCreatedByAddingNewItemToExistingSubsets(
            List<SetWithContainedSetsInfo<E>> existingSubsets, SetWithName<E> newItem
    ) {
        ArrayList<SetWithContainedSetsInfo<E>> subsetsWithNewItem = new ArrayList<>();
        for (SetWithContainedSetsInfo<E> subset : existingSubsets) {
            SetWithContainedSetsInfo<E> newSubset = new SetWithContainedSetsInfo<>(subset);
            newSubset.addAll(newItem);
            subsetsWithNewItem.add(newSubset);
        }
        return subsetsWithNewItem;
    }
}
