package generators;


import model.SetWithName;

import java.util.*;

public class SetsGenerator {
    private Integer sizeOfTheProblem;
    private Integer setElementMinValue;
    private Integer setElementMaxValue;
    private Set<Integer> u;
    private List<Integer> uValues;
    private List<SetWithName<Integer>> subsetsList;
    private Random random;

    public SetsGenerator(Integer sizeOfTheProblem, Integer setElementMinValue, Integer setElementMaxValue) {
        this.sizeOfTheProblem = sizeOfTheProblem;
        this.setElementMinValue = setElementMinValue;
        this.setElementMaxValue = setElementMaxValue;

        if(!isProblemCalculable())
            return;

        this.random = new Random();
        uValues = new ArrayList<>();

        generateUSet();

        this.subsetsList = new ArrayList<>();
        generateSubsets();
    }

    public Boolean isProblemCalculable(){
        return (setElementMaxValue - setElementMinValue + 1 >= sizeOfTheProblem);
    }

    private void generateUSet(){
        int generatedValue = generateRandomValue(setElementMinValue, setElementMaxValue);

        for(int i=0; i<sizeOfTheProblem; i++) {
            while(uValues.contains(generatedValue))
                generatedValue = generateRandomValue(setElementMinValue, setElementMaxValue);

            uValues.add(generatedValue);
            generatedValue = generateRandomValue(setElementMinValue, setElementMaxValue);
        }

        u = new TreeSet<>(uValues);
    }

    private void generateSubsets(){
        SetWithName<Integer> si;
        List<Integer> subsetValues = new ArrayList<>();
        StringBuilder sb = new StringBuilder("s");
        Integer generatedRandomValue;
        int numberOfSubsets;
        Set<Integer> uniqueValuesInSubsets = new TreeSet<>();

        if(sizeOfTheProblem % 2 == 0)
            numberOfSubsets = sizeOfTheProblem/2;
        else
            numberOfSubsets = (sizeOfTheProblem - 1) / 2;

        for(int i=0; i<numberOfSubsets; i++){
            int numberOfElementsInSubset = generateRandomValue(1, numberOfSubsets);
            for(int j=0; j<numberOfElementsInSubset; j++){
                generatedRandomValue = generateRandomValue(setElementMinValue, setElementMaxValue);

                while(subsetValues.contains(generatedRandomValue))
                    generatedRandomValue = generateRandomValue(setElementMinValue, setElementMaxValue);

                subsetValues.add(generatedRandomValue);
                uniqueValuesInSubsets.add(generatedRandomValue);
            }

            si = new SetWithName<>(sb.append(i+1).toString(), subsetValues);
            subsetsList.add(si);
            subsetValues.clear();
            sb = new StringBuilder("s");
        }

        Set<Integer> relativeComplementSet = getRelativeComplementWithU(uniqueValuesInSubsets);

        int differenceBetweenSets = 1;
        if(relativeComplementSet.size() > subsetsList.size()){
            differenceBetweenSets = relativeComplementSet.size() - subsetsList.size();
        }

        SetWithName<Integer> currentSi;
        int index = 0;

        for(Integer i : relativeComplementSet){
            if(differenceBetweenSets >= 0){
                currentSi = subsetsList.get(index);
                currentSi.add(i);
                subsetsList.set(index, currentSi);
                differenceBetweenSets--;
            }
            else{
                index++;
                currentSi = subsetsList.get(index);
                currentSi.add(i);
                subsetsList.set(index, currentSi);
            }
        }
    }

    private Set<Integer> getRelativeComplementWithU(Set<Integer> s){
        Set<Integer> relativeComplementSet = new TreeSet<>();

        for(Integer i : u) {
            if (!s.contains(i))
                relativeComplementSet.add(i);
        }

        return relativeComplementSet;
    }

    private Integer generateRandomValue(Integer lowerLimit, Integer upperLimit){
        return lowerLimit + random.nextInt(upperLimit - lowerLimit + 1);
    }

    public Set<Integer> getU() {
        return u;
    }

    public List<SetWithName<Integer>> getSubsetsList() {
        return subsetsList;
    }
}
